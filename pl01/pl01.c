#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#define ARRAY_SIZE 1000



void pl1_ex01() {
	int x = 1;
	pid_t p = fork(); /*pid_t: sys/types.h; fork(): unistd.h*/
	if (p == 0) { // Filho
		x = x + 1;
		printf("1. x = %d\n", x);
	} else { // Pai
		x = x - 1;
		printf("2. x = %d\n", x);
	}
	printf("3. %d; x = %d\n", p, x);
}
//if dad will print 2 then 3, if son will print 1 then 3

void pl1_ex02() {
	printf("I'm..\n");
	if (fork() != 0) { // Pai
		wait(NULL); // Pai espera que o 1º filho termine
		printf("..the..\n"); // O pai esperou que o 1º filho terminasse para escrever
		if (fork() != 0) { // Pai
			wait(NULL); // Pai espera que o 2º filho termine
			printf("father!\n"); // O pai esperou que o 2º filho terminasse para escrever
		} else { // 2º Filho criado
			printf("I'll never join you!\n");
			exit(0); // 2º Filho termina
		}
	} else { // 1º Filho criado
		printf("I'll never join you!\n");
		exit(0); // 1º Filho termina
	}
	printf("I'll never join you!\n"); // Com os dois filhos terminados, apenas o pai escreve
}

void pl1_ex03() {
	fork();
	fork();
	fork();
	printf("SCOMP!\n");
}
//a) 8 processos
//c) 8 vezes

void pl1_ex04() {
	int a = 0, b, c, d;
	b = (int) fork();
	c = (int) getpid(); /* getpid(), getppid(): unistd.h*/
	d = (int) getppid();
	a = a + 5;
	if (b > 0)
		wait(NULL); //assim o pai espera pelo filho acabar
	printf("\na=%d, b=%d, c=%d, d=%d\n", a, b, c, d);
}

void pl1_ex05() {
	int status;
	if (fork() > 0) { // Pai
		wait(&status); // Pai espera que o 1º filho termine
		if (WIFEXITED(status)) {
			printf("1st son exit status: %d\n", WEXITSTATUS(status));
			// O pai esperou que o 1º filho terminasse para escrever
		}

		if (fork() > 0) { // Pai
			wait(&status); // Pai espera que o 2º filho termine
			if (WIFEXITED(status)) {
				printf("2nd son exit status: %d\n", WEXITSTATUS(status));
				// O pai esperou que o 2º filho terminasse para escrever
			}
		} else { // 2º Filho criado
			sleep(2);
			exit(2); // 2º Filho termina
		}
	} else { // 1º Filho criado
		sleep(1);
		exit(1); // 1º Filho termina
	}
}

int search_for_n(int size,int* numbers,int n){
	int status;
	int i = 0;
	int dad_count=0;
	int son_count=0;
	int result = 0;

	if (fork() > 0) { // Pai
		for(i=0;i<size/2;i++){
			if(numbers[i]==n){
				dad_count++;
			}
		}
		wait(&status);
		if (WIFEXITED(status)) {
			result = dad_count + WEXITSTATUS(status);

			printf("Numero de vezes que %d foi encontrado: %d\n",n,result);
			// O pai esperou que o 1º filho terminasse para escrever

		}

	} else { // 1º Filho criado
		int j=0;
		for(j=size/2;j<size;j++ ){
			if(numbers[j]==n){
				son_count++;
			}
		}
		exit(son_count); // 1º Filho termina
	}

	return result;
}

void pl1_ex07() {
	int numbers[ARRAY_SIZE]; /* array to lookup */
	int n; /* the number to find */
	time_t t; /* needed to init. the random number generator (RNG)
	 */
	int i;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time(&t));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < ARRAY_SIZE; i++)
		numbers[i] = rand() % 10000;

	/* initialize n */
	n = rand() % 10000;

	int result=0;

	int teste_array[10]={1,2,3,4,5,6,7,8,9,1};
	result = search_for_n(10,teste_array,1);

	result = search_for_n(ARRAY_SIZE,numbers,n);


}

void pl1_ex09() {
	int i=0;
	int j=0;
	int k=0;
	int p=0;
	int s=0;
	int lower_limit=1;
	int upper_limit=100;
	for(i=0;i<10;i++){
		p=fork();
		if(p==0){
			for(j=lower_limit;j<=upper_limit;j++){
				printf("%d;",j);
			}
			printf("\n");

			exit(0);
		}
		lower_limit = upper_limit + 1;
		upper_limit +=100;
	}

	for(k=0;k<10;k++){
		wait(&s);
	}
}
//Não se pode garantir a ordem porque não é possível garantir a ordem pelo qual os processos são
//executados.

void pl1_ex10() {
	int array_size = 2000;
	int numbers[array_size]; /* array to lookup */
	int n; /* the number to find */
	n=4;
	time_t t; /* needed to init. the random number generator (RNG)
	 */
	int a;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time(&t));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (a = 0; a < ARRAY_SIZE; a++){
		numbers[a] = rand() % 10000;
	}
	int status=0;
	int i=0;
	int j=0;
	int k=0;
	int p=0;
	int s=0;
	int lower_limit=0;
	int upper_limit=200;
	int rel_pos;
	for(i=1;i<=10;i++){
		p=fork();
		if(p==0){
			rel_pos = 255;
			for(j=lower_limit;j<=upper_limit;j++){
				if(numbers[j]==n){
					rel_pos = j/i;
					break;
				}
			}
			exit(rel_pos);
		}
		lower_limit = upper_limit + 1;
		upper_limit +=200;
	}

	for(k=0;k<10;k++){
		wait(&s);
		if (WIFEXITED(s)) {
			printf("%d found in: %d\n",n, WEXITSTATUS(s));
		}

	}


}

void pl1_ex11(){
	int CHILD_COUNT =5;
	int numbers[ARRAY_SIZE];    /* array to lookup */
	int result[ARRAY_SIZE];
	int p[CHILD_COUNT];
	int e[CHILD_COUNT];
	time_t t;            /*needed to initialize random number generator (RNG) */
	int i, lower_limit = 0, upper_limit = ARRAY_SIZE/CHILD_COUNT, aux = -1;

	/*intializes RNG (srand():stdlib.h; time(): time.h) */
	srand ((unsigned) time (&t));

	/*initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < ARRAY_SIZE; i++){
		numbers[i] = rand () % 255;
	}

	for (i = 0; i < CHILD_COUNT; i++){
		p[i] = fork();
		if(p[i] == 0){
			for (i = lower_limit; i < lower_limit + upper_limit; i++) {
				if (numbers[i] > aux) {
					aux = numbers[i];
				}
			}
			exit(aux);
		}
		lower_limit += upper_limit;
	}

	for (i = 0; i < CHILD_COUNT; i++){
		waitpid(p[i], &e[i],0);
		if( WEXITSTATUS(e[i]) > aux){
			aux = WEXITSTATUS(e[i]);
		}
	}

	if(fork() == 0){
		for (i = 0; i < ARRAY_SIZE/2; i++) {
			result[i]=((int) (numbers[i])/aux)*100;
			printf("i:%d %d\n",i, result[i]);
		}
		exit(0);
	}else{
		for (i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++) {
			result[i]=((int) numbers[i]/aux)*100;
		}
		wait(NULL);
		for (i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++) {
			printf("i:%d %d\n",i, result[i]);
		}
	}
}

int main() {
	//pl1_ex01();
	//pl1_ex02();
	//pl1_ex03();
	//pl1_ex04();
	//pl1_ex05();
	//pl1_ex06();
	//pl1_ex07();
	//pl1_ex08();
	//pl1_ex09();
	//pl1_ex10();
	pl1_ex11();
	//pl1_ex12(); Problemas na interpretação do enunciado

	return 0;
}
