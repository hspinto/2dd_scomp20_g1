#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <stdbool.h>

void pl2_ex1() {
	int fd[2];  //descritor
	pid_t p;

	//criar o pipe, antes de criar o filho, para ser o mesmo pipe no pai e no filho
	if(pipe(fd)==-1){
		perror("Pipe failed");
		//return 0;
	}

	if(fork()==0){ //filho
		close(fd[1]); //fecha a escrita
		read(fd[0], &p, sizeof(p)); //lê do pipe com o descritor (ESPERA que exista algo para ler)
		printf("O filho leu do pipe: %d\n", p); //escreve no ecra
		close(fd[0]); //fecha a leitura
		exit(0);
	}
	else{
		p=getpid();
		printf("Pai: %d\n", getpid());
		close(fd[0]); //fecha a leitura
		write(fd[1], &p, sizeof(p)); //escreve no pipe com o descritor
		close(fd[1]); //fecha a escrita
	}
	wait(NULL);
}

void pl2_ex2a() {
	int fd[2];  //descritor
	char frase[50];
	int valor;

	pipe(fd); //pipe, antes de criar o filho, para ser o mesmo pipe no pai e no filho

	if(fork()==0){ //filho
		close(fd[1]); //fecha a escrita
		read(fd[0], &valor, sizeof(valor));
		read(fd[0], &frase, sizeof(frase)+1);
		printf("O filho leu do pipe o valor: %d\n", valor);
		printf("O filho leu do pipe o valor: %s\n", frase);
		close(fd[0]); //fecha a leitura
		exit(0);
	}
	else{
		printf("Qual o valor numérico?"); scanf("%d", &valor);
		printf("Qual a frase?"); fflush(stdin); scanf("%s", frase); fflush(stdin);
		close(fd[0]); //fecha a leitura
		write(fd[1], &valor, sizeof(valor));
		write(fd[1], frase, sizeof(frase)+1);
		close(fd[1]); //fecha a escrita
	}
	wait(NULL);
}

void pl2_ex2b() {
	int fd[2];  //descritor
	struct sconteudo {
		char frase[50];
		int valor;
	} conteudo;

	pipe(fd); //pipe, antes de criar o filho, para ser o mesmo pipe no pai e no filho

	if(fork()==0){ //filho
		close(fd[1]); //fecha a escrita
		read(fd[0], &conteudo.valor, sizeof(conteudo.valor));
		read(fd[0], &conteudo.frase, sizeof(conteudo.frase)+1);
		printf("O filho leu do pipe o valor: %d\n", conteudo.valor);
		printf("O filho leu do pipe o valor: %s\n", conteudo.frase);
		close(fd[0]); //fecha a leitura
		exit(0);
	}
	else{
		printf("Qual o valor numérico?"); scanf("%d", &conteudo.valor);
		printf("Qual a frase?"); fflush(stdin); scanf("%s", conteudo.frase); fflush(stdin);
		close(fd[0]); //fecha a leitura
		write(fd[1], &conteudo.valor, sizeof(conteudo.valor));
		write(fd[1], conteudo.frase, sizeof(conteudo.frase)+1);
		close(fd[1]); //fecha a escrita
	}
	wait(NULL);
}

void pl2_ex3(){
	int fd[2];  //descritor
	int s;
	char frase[50];
	char frase2[50];
	char f1[50] ="Hello World";
	char f2[50] ="Goodbye!";
	pipe(fd); //pipe, antes de criar o filho, para ser o mesmo pipe no pai e no filho

	pid_t pid = fork();

	if(pid==0){ //filho
		close(fd[1]); //fecha a escrita
		read(fd[0], &frase, sizeof(frase)+1);
		read(fd[0], &frase2, sizeof(frase2)+1);
		printf("O filho leu do pipe o valor: %s\n", frase);
		printf("O filho leu do pipe o valor: %s\n", frase2);
		close(fd[0]); //fecha a leitura
		exit(0);

	}
	else{
		//	printf("Qual a frase?"); fflush(stdin); scanf("%s", frase); fflush(stdin);
		//	printf("Qual a frase?"); fflush(stdin); scanf("%s", frase2); fflush(stdin);

		close(fd[0]); //fecha a leitura
		write(fd[1], f1, sizeof(f1)+1);
		write(fd[1], f2, sizeof(f2)+1);
		close(fd[1]); //fecha a escrita
	}
	wait(NULL);
	if (WIFEXITED(s)) {
		printf("child p_id:%d child exit status: %d\n",pid, WEXITSTATUS(s));
	}

}

void pl2_ex4() {

	int fd[2];  //descritor
	char frase[50];

	pipe(fd); //pipe, antes de criar o filho para ser o mesmo

	if(fork()==0){ //filho
		close(fd[1]); //fecha a escrita
		read(fd[0], frase, sizeof(frase)+1); //le do pipe com o descritor
		printf("O filho leu do pipe a string: %s\n", frase); //escreve no ecra
		close(fd[0]); //fecha a leitura
		exit(0);
	}
	else{
		close(fd[0]); //fecha a leitura
		int c, i=0;
		FILE *fp=fopen("/home/hsp/Desktop/fich.txt", "r"); //ALTERAR PWD DO FICH!!!
		while(1) {
			c = fgetc(fp);
			if(feof(fp)) {
				break ;
			}
			frase[i]=c;
			i++;
		}
		fclose(fp);
		write(fd[1], frase, sizeof(frase) + 1); //escreve no pipe com o descritor
		close(fd[1]); //fecha a escrita
	}
	wait(NULL);
}

void pl2_ex5(){
	int fdUP[2];
	int fdDN[2];
	char frase[255];
	char f1[255];
	char f2[255];


	char* fptr;
	pipe(fdUP); //pipe, antes de criar o filho, para ser o mesmo pipe no pai e no filho
	pipe(fdDN); //pipe, antes de criar o filho, para ser o mesmo pipe no pai e no filho


	if(fork()==0){ //filho
		printf("Qual a frase?\n"); fflush(stdin); scanf("%[^\n]s", frase); fflush(stdin);
		close(fdDN[1]); //fecha a escrita no DOWN
		close(fdUP[0]); //fecha a read no UP

		write(fdUP[1], frase, sizeof(frase)+1);

		read(fdDN[0], &f2, sizeof(f2)+1);

		printf("O filho leu do pipe o valor: %s\n", f2);

		exit(0);
	}
	else{ //pai
		close(fdDN[0]); //fecha a read no DOWN
		close(fdUP[1]); //fecha a write no UP
		read(fdUP[0], &f1, sizeof(f1)+1);

		fptr = f1;
		int i;
		while(*fptr){
			if(*fptr>='a' && *fptr<='z'){
				*fptr -= 32;
			}
			else if(*fptr>='A' && *fptr<='Z'){
				*fptr += 32;
			}
			fptr++;
		}

		write(fdDN[1], f1, sizeof(f1)+1);
	}
	wait(NULL);
}

int vec_sum200(int n,int *vec1 ,int *vec2){
	int sum=0;
	int limit = 200;
	int i;
	limit += n;
	for(i=n;i<limit;i++){
		sum += vec1[i]+ vec2[i];
	}

	return sum;
}

int vec_sum_equal_vec(int size, int *vec1 , int *vec2){
	int i =0;
	int sum = 0;
	for(i=0;i<size;i++){
		sum += vec1[i] + vec2[i];
		printf("sum at i:%d = %d\n",i,sum);
	}

	return sum;
}

void pl2_ex6(){
	//###################VETORES ALEATORIOS DE 1000(pedido pelo enunciado)################

	int ARRAY_SIZE=1000;
	int vec1[ARRAY_SIZE];
	int vec2[ARRAY_SIZE];

	time_t z; /* needed to init. the random number generator (RNG)
	 */
	int k;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time(&z));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (k = 0; k < ARRAY_SIZE; k++)
		vec1[k] = rand() % 10000;

	for (k = 0; k < ARRAY_SIZE; k++)
		vec2[k] = rand() % 10000;

	//###################VETORES FIXOS PARA TESTE(alterar a funçao de sum) ##########
	//	int vec1[]={1,2,3,4,5};
	//	int vec2[] ={1,2,3,4,5};
	//#####################################

	int fd[2];
	int p=0;
	int limit=0;
	int total_sum=0;

	pipe(fd);

	int temp=0;
	int t;
	for(t=0;t<5;t++){
		p = fork();
		if(p==0){  //filho
			temp = vec_sum200(limit,vec1,vec2);
			//temp = vec_sum_equal_vec(5,vec1,vec2); //different function to test with smaller array sizes;
			write(fd[1], &temp, sizeof(temp));
			limit +=200;
			exit(0);
		}
		else {
			int partial_sum=0;
			read(fd[0],&partial_sum,sizeof(int));
			total_sum+=partial_sum;

		}

	}
	int s;
	int r;
	for(r=0;r<5;r++){
		wait(&s);
	}

	printf("Total Sum: %d",total_sum);

}


void vec_sum_ex7(int n,int *vec1 ,int *vec2,int *partial_vec){
	int limit = 200;
	int i;
	limit += n;
	int index=0;
	for(i=n;i<limit;i++){
		partial_vec[index] = vec1[i]+ vec2[i];
		index++;
	}

}

void add_to_array(int lower, int upper , int *partial , int *result){
	int i;
	int index=0;
	for(i=lower;i<upper;i++){
		result[i]=partial[index];
		index++;

	}
}

void array_sort(int size,int *v){
	int i;
	int j;
	int temp=0;
	for(i=0;i<size;i++){
		for(j=i+1;j<size;j++){
			if(v[i]>v[j]){
				temp=v[i];
				v[i]=v[j];
				v[j]=temp;
			}
		}
	}
}

void pl2_ex7(){
	//###################VETORES ALEATORIOS DE 1000(pedido pelo enunciado)################

	int ARRAY_SIZE=1000;
	int vec1[ARRAY_SIZE];
	int vec2[ARRAY_SIZE];

	time_t z; /* needed to init. the random number generator (RNG)
	 */
	int k;

	/* intializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time(&z));

	/* initialize array with random numbers (rand(): stdlib.h) */
	for (k = 0; k < ARRAY_SIZE; k++)
		vec1[k] = rand() % 10000;

	for (k = 0; k < ARRAY_SIZE; k++)
		vec2[k] = rand() % 10000;

	//###################VETORES FIXOS PARA TESTE(alterar a funçao de sum) ##########
	//	int vec1[]={1,2,3,4,5};
	//	int vec2[] ={1,2,3,4,5};
	//#####################################

	int fd1[2];
	int fd2[2];
	int fd3[2];
	int fd4[2];
	int fd5[2];


	pipe(fd1);
	pipe(fd2);
	pipe(fd3);
	pipe(fd4);
	pipe(fd5);

	int d;


	int limit=0;
	int partial_vec[200];
	int result_vec[1000] = {-1};

	int status;

	if (fork() > 0) { // Pai
		wait(&status); // Pai espera que o 1º filho termine

		if (fork() > 0) { // Pai
			wait(&status); // Pai espera que o 2º filho termine

			if (fork() > 0) { // Pai
				wait(&status); // Pai espera que o 3º filho termine

				if (fork() > 0) { // Pai
					wait(&status); // Pai espera que o 4º filho termine

					if (fork() > 0) { // Pai
						wait(&status); // Pai espera que o 5º filho termine
						int p1[200];
						int p2[200];
						int p3[200];
						int p4[200];
						int p5[200];
						read(fd1[0],&p1,sizeof(p1));

						read(fd2[0],&p2,sizeof(p2));

						read(fd3[0],&p3,sizeof(p3));
						read(fd4[0],&p4,sizeof(p4));
						read(fd5[0],&p5,sizeof(p5));

						add_to_array(0,200,p1,result_vec);
						add_to_array(200,400,p2,result_vec);
						add_to_array(400,600,p3,result_vec);
						add_to_array(600,800,p4,result_vec);
						add_to_array(800,1000,p5,result_vec);
						//						printf("\n");
						int a = 0;
						for(a=0;a<1000;a++){
							printf("vec1[%d]=%d+vec2[%d]=%d = %d\n",a,vec1[a],a,vec2[a],result_vec[a]);
						}

						//array_sort(1000,result_vec);




					} else { // 5º Filho criado
						vec_sum_ex7(800,vec1,vec2,partial_vec);
						write(fd5[1],partial_vec,sizeof(partial_vec));
						exit(2); // 5º Filho termina
					}

				} else { // 4º Filho criado
					vec_sum_ex7(600,vec1,vec2,partial_vec);
					write(fd4[1],partial_vec,sizeof(partial_vec));;
					exit(2); // 4º Filho termina
				}

			} else { // 3º Filho criado
				vec_sum_ex7(400,vec1,vec2,partial_vec);
				write(fd3[1],partial_vec,sizeof(partial_vec));
				exit(2); // 3º Filho termina
			}

		} else { // 2º Filho criado
			vec_sum_ex7(200,vec1,vec2,partial_vec);
			write(fd2[1],partial_vec,sizeof(partial_vec));
			exit(2); // 2º Filho termina
		}
	} else { // 1º Filho criado
		vec_sum_ex7(0,vec1,vec2,partial_vec);
		write(fd1[1],partial_vec,sizeof(partial_vec));
		exit(1); // 1º Filho termina
	}


}

void pl2_ex8(){
	int child_count = 10;

	int p[child_count];
	int e[child_count];
	int i;
	int fd[2];

	typedef struct {
		char message[5];
		int roundnum;
	} Game;

	Game s;

	pipe(fd);

	for (i = 0; i < child_count; i++){
		p[i]=fork();
		if(p[i]>0){
			//Game s1;
			close(fd[0]);
			sleep(1);
			strcpy(s.message, "Win");
			s.roundnum=i+1;
			//			write(fd[1],&s,sizeof(s)); Faz com que a ronda 1 seja impressa por dois child
			write(fd[1],&s,sizeof(&s));

			close(fd[1]);


		}else{
			//Game s2;
			close(fd[1]);
			read(fd[0],&s,sizeof(Game));
			close(fd[0]);
			//			printf("Win message=%s, round number=%d	\n",s2.message,s2.roundnum);
			exit(s.roundnum);
		}
	}

	int r;
	for(r=0;r<child_count;r++){
		waitpid(p[r], &e[r],0);
	}

	for(r=0;r<child_count;r++){
		printf("child: %d won round %d\n",p[r],WEXITSTATUS(e[r]));
	}

}

void pl2_ex9(){
	int i;
	int CHILD_COUNT = 10;
	int ARRAY_SIZE = 50000;
	int lower_limit = 0;
	int upper_limit = 5000;
	int fd[2];
	int k=0;

	time_t t;
	int products[100];
	int z;
	for(z=0;z<100;z++){
		products[z]=-1;
	}

	pipe(fd);

	typedef struct{
		int customer_code;
		int product_code;
		int quantity;
	} Product;

	Product product_array[ARRAY_SIZE];

	/*intializes RNG (srand():stdlib.h; time(): time.h) */
	srand ((unsigned) time (&t));

	/*initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < ARRAY_SIZE; i++){
		Product p;
		p.customer_code = rand ()  % 5000 +1;
		p.product_code = rand() % 2000 +1;
		p.quantity = rand()  %1000 +1;
		product_array[i] = p;
	}


	for (i = 0; i < CHILD_COUNT; i++){
		if(fork() == 0){
			for (i = lower_limit; i < lower_limit + upper_limit; i++) {
				//printf("quantity:%d\n",product_array[i].quantity);
				if(product_array[i].quantity>20){
					close(fd[0]);
					int product_code = product_array[i].product_code;
					printf("child: %d pro_code:%d\n",getpid(),product_code);
					write(fd[1],&product_code,sizeof(product_code));
				}

			}
			lower_limit += upper_limit;
			exit(0);
		}
		else{
			int code;
			close(fd[1]);
			read(fd[0],&code,sizeof(code));
			printf("pro_code:%d\n",code);
			products[k] = code;
			k++;
		}
	}

	//	int s;
	//	int r;
	//	for (r = 0; r < CHILD_COUNT; r++){
	//		wait(&s);
	//	}
	int a;
	for (a = 0; a < 100; a++){
		printf("product code :%d\n",products[a]);
	}

}

void pl2_ex10(){
	int number;
	int fd2child[2];
	int fd2parent[2];
	int child_credit = 20;
	int one=1;
	int zero=0;
	bool x = true;
	time_t t;

	pipe(fd2child);
	pipe(fd2parent);

	pid_t p;
	p=fork();

	do{
		/*intializes RNG (srand():stdlib.h; time(): time.h) */
		srand ((unsigned) time (&t));

		/*initialize array with random numbers (rand(): stdlib.h) */
		number = rand ()% 5 + 1;
		sleep(1);

		if(p>0){
			if(child_credit>0){
				write(fd2child[1],&one,sizeof(int));
			}else{
				x=false;
				write(fd2child[1],&zero,sizeof(int));
			}
			int child_bet;
			read(fd2parent[0],&child_bet,sizeof(child_bet));
			if(child_bet==number){
				child_credit+=10;
			}else{
				child_credit-=5;
			}
			write(fd2child[1],&child_credit,sizeof(child_credit));

		}else{
			int status;
			read(fd2child[0],&status,sizeof(status));
			if(status==1){
				int cn;
				cn= rand ()% 5 + 1;
				write(fd2parent[1],&cn,sizeof(cn));

			}
			int curr_credit;
			read(fd2child[0],&curr_credit,sizeof(curr_credit));
			printf("current credit:%d\n",curr_credit);
			sleep(1);
		}
	}while(child_credit >0);
}

int randomNumber(int min,int max){
	time_t t;
	srand ((unsigned) time (&t));
	return (rand ()% max + min);
}

void pl2_ex11(){
	int parent2one [2];
	int one2two[2];
	int two2three[2];
	int three2four[2];
	int four2five[2];
	int five2parent[2];
	int child_count = 5;


	pipe(parent2one);
	pipe(one2two);
	pipe(two2three);
	pipe(three2four);
	pipe(four2five);
	pipe(five2parent);

	int status;

	if (fork() > 0) { // Pai
		int num;
		num = randomNumber(1,500);
		sleep(2);
		printf("PID:%d RNG:%d\n",getpid(),num);
		write(parent2one[1],&num,sizeof(num));
		wait(&status); // Pai espera que o 1º filho termine

		if (fork() > 0) { // Pai
			wait(&status); // Pai espera que o 2º filho termine

			if (fork() > 0) { // Pai
				wait(&status); // Pai espera que o 3º filho termine

				if (fork() > 0) { // Pai
					wait(&status); // Pai espera que o 4º filho termine

					if (fork() > 0) { // Pai
						wait(&status); // Pai espera que o 5º filho termine
						int receivedNumber;
						read(five2parent[0],&receivedNumber,sizeof(int));
						printf("greatest Number: %d\n",receivedNumber);

					} else { // 5º Filho criado
						int num5;
						num5 = randomNumber(1,500);
						sleep(2);
						printf("PID:%d RNG:%d\n",getpid(),num5);
						int receivedNumber;
						read(four2five[0],&receivedNumber,sizeof(int));
						if(num5>receivedNumber){
							write(five2parent[1],&num5,sizeof(int));
						}else{
							write(five2parent[1],&receivedNumber,sizeof(int));

						}
						exit(2); // 5º Filho termina
					}

				} else { // 4º Filho criado
					int num4;
					num4 = randomNumber(1,500);
					printf("PID:%d RNG:%d\n",getpid(),num4);
					int receivedNumber;
					read(three2four[0],&receivedNumber,sizeof(int));
					if(num4>receivedNumber){
						write(four2five[1],&num4,sizeof(int));
					}else{
						write(four2five[1],&receivedNumber,sizeof(int));
					}
					exit(2); // 4º Filho termina
				}

			} else { // 3º Filho criado
				int num3;
				num3 = randomNumber(1,500);
				sleep(2);
				printf("PID:%d RNG:%d\n",getpid(),num3);
				int receivedNumber;
				read(two2three[0],&receivedNumber,sizeof(int));
				if(num3>receivedNumber){
					write(three2four[1],&num3,sizeof(int));
				}else{
					write(three2four[1],&receivedNumber,sizeof(int));
				}
				exit(2); // 3º Filho termina
			}

		} else { // 2º Filho criado
			int num2;
			num2 = randomNumber(1,500);
			sleep(2);
			printf("PID:%d RNG:%d\n",getpid(),num2);
			int receivedNumber;
			read(one2two[0],&receivedNumber,sizeof(int));
			if(num2>receivedNumber){
				write(two2three[1],&num2,sizeof(int));
			}else{
				write(two2three[1],&receivedNumber,sizeof(int));
			}
			exit(2); // 2º Filho termina
		}


	} else { // 1º Filho criado
		int num1;
		num1 = randomNumber(1,500);
		sleep(2);
		printf("PID:%d RNG:%d\n",getpid(),num1);
		int receivedNumber;
		read(parent2one[0],&receivedNumber,sizeof(int));
		if(num1>receivedNumber){
			write(one2two[1],&num1,sizeof(int));
		}else{
			write(one2two[1],&receivedNumber,sizeof(int));
		}
		exit(1); // 1º Filho termina
	}


}


void pl2_ex12(){

	typedef struct{
		char product_name[50];
		float product_price;
		int barcode;
	} Product;


	int info[2];
	int fd1[2];
	int fd2[2];
	int fd3[2];
	int fd4[2];
	int fd5[2];

	pipe(info);
	pipe(fd1);
	pipe(fd2);
	pipe(fd3);
	pipe(fd4);
	pipe(fd5);



	int status;

	if (fork() > 0) { // Pai
		Product p1,p2,p3,p4;
		strcpy(p1.product_name,"Arroz");
		strcpy(p2.product_name,"Agua");
		strcpy(p3.product_name,"Papel Higiénico");
		strcpy(p4.product_name,"Atum");
		p1.product_price=2;
		p2.product_price=1.2;
		p3.product_price=50;
		p4.product_price=2.5;
		p1.barcode=77777;
		p2.barcode=12345;
		p3.barcode=66521;
		p4.barcode=33333;
		Product products[]={p1,p2,p3,p4};
		int prod_num = 4;
		int i;

		int bc;

		read(info[0],&bc,sizeof(int));
		for(i=0;i<prod_num;i++){
			if(products[i].barcode==bc){
				write(fd1[1],&products[i],sizeof(products[i]));
			}
		}

		wait(&status); // Pai espera que o 1º filho termine

		if (fork() > 0) { // Pai
			int i2;

			int bc2;

			read(info[0],&bc2,sizeof(int));
			for(i2=0;i2<prod_num;i2++){
				if(products[i2].barcode==bc2){
					write(fd2[1],&products[i2],sizeof(products[i2]));
				}
			}
			wait(&status); // Pai espera que o 2º filho termine

			if (fork() > 0) { // Pai
				int i3;

				int bc3;

				read(info[0],&bc3,sizeof(int));
				for(i3=0;i3<prod_num;i3++){
					if(products[i3].barcode==bc3){
						write(fd3[1],&products[i3],sizeof(products[i3]));
					}
				}
				wait(&status); // Pai espera que o 3º filho termine

				if (fork() > 0) { // Pai
					int i4;

					int bc4;

					read(info[0],&bc4,sizeof(int));
					for(i4=0;i4<prod_num;i4++){
						if(products[i4].barcode==bc4){
							write(fd4[1],&products[i4],sizeof(products[i4]));
						}
					}
					wait(&status); // Pai espera que o 4º filho termine

					if (fork() > 0) { // Pai
						int i5;

						int bc5;

						read(info[0],&bc5,sizeof(int));
						for(i5=0;i5<prod_num;i5++){
							if(products[i5].barcode==bc5){
								write(fd5[1],&products[i5],sizeof(products[i5]));
							}
						}
						wait(&status); // Pai espera que o 5º filho termine

					} else { // 5º Filho criado
						int n=66521;
						write(info[1],&n,sizeof(n));
						Product p;
						read(fd5[0],&p,sizeof(p));
						printf("Barcode:%d \nName:%s \nPrice:%f\n",p.barcode,p.product_name,p.product_price);
						exit(2); // 5º Filho termina
					}

				} else { // 4º Filho criado
					int n=33333;
					write(info[1],&n,sizeof(n));
					Product p;
					read(fd4[0],&p,sizeof(p));
					printf("Barcode:%d \nName:%s \nPrice:%f\n",p.barcode,p.product_name,p.product_price);
					exit(2); // 4º Filho termina
				}

			} else { // 3º Filho criado
				int n=66521;
				write(info[1],&n,sizeof(n));
				Product p;
				read(fd3[0],&p,sizeof(p));
				printf("Barcode:%d \nName:%s \nPrice:%f\n",p.barcode,p.product_name,p.product_price);
				exit(2); // 3º Filho termina
			}

		} else { // 2º Filho criado
			int n=12345;
			write(info[1],&n,sizeof(n));
			Product p;
			read(fd2[0],&p,sizeof(p));
			printf("Barcode:%d \nName:%s \nPrice:%f\n",p.barcode,p.product_name,p.product_price);
			exit(2); // 2º Filho termina
		}
	} else { // 1º Filho criado
		int n=77777;
		write(info[1],&n,sizeof(n));
		Product p;
		read(fd1[0],&p,sizeof(p));
		printf("Barcode:%d \nName:%s \nPrice:%f\n",p.barcode,p.product_name,p.product_price);
		exit(1); // 1º Filho termina
	}



}

void pl2_ex13(){
	int m1m2[2];
	int m2m3[2];
	int m3m4[2];
	int m4a1[2];


	pipe(m1m2);
	pipe(m2m3);
	pipe(m3m4);
	pipe(m4a1);

	int total_pieces=300;
	int stored_parts = 0;
	int cutPieces = 0;
	int foldPieces = 0;
	int weldPieces= 0;
	int packPieces= 0;
	int status;
	int e;
	int s;
	int child_count=4;
	int p[child_count];


	if (fork() > 0) { // Pai
		if (fork() > 0) { // Pai
			if (fork() > 0) { // Pai
				if (fork() > 0) { // Pai
				} else { // 4º Filho criado
					p[3]=getpid();
				}
			} else { // 3º Filho criado
				p[2]=getpid();
			}
		} else { // 2º Filho criado
			p[1]=getpid();
		}
	} else { // 1º Filho criado

		p[0]=getpid();
	}

	//	int m;
	//	for(m=0;m<child_count;m++){
	//		printf("p[%d]:%d\n",m,p[m]);
	//	}

	do{
		//printf("pid: %d\n",getpid());
		if(getpid()==p[3]){ // 4º Filho criado

			int read1 = 0;
			read(m3m4[0],&read1,sizeof(read1));
			packPieces += read1;
			if(packPieces>=100){
				total_pieces -=100;
				write(m4a1[1],&packPieces,sizeof(packPieces));
				packPieces -= 100;
			}



		} else if(getpid()==p[2]){ // 3º Filho criado


			int read3 = 0;
			read(m2m3[0],&read3,sizeof(read3));
			weldPieces += read3;
			printf("weldPieces: %d\n",weldPieces);


			if(weldPieces >=10){
				total_pieces -=10;
				write(m3m4[1],&weldPieces,sizeof(weldPieces));
				weldPieces -= 10;
			}



		} else if(getpid()==p[1]) { // 2º Filho criado
			total_pieces -=5;

			int scan =0;
			read(m1m2[0],&scan,sizeof(scan));
			foldPieces +=scan;
			printf("foldPieces: %d\n",foldPieces);
			write(m2m3[1],&foldPieces,sizeof(foldPieces));
			foldPieces -=5;



		} else if(getpid()==p[0]) { // 1º Filho criado
			sleep(1);
			total_pieces -=5;
			printf("Total pieces:%d\n",total_pieces);
			cutPieces +=5;
			write(m1m2[1],&cutPieces,sizeof(cutPieces));
			printf("cutPieces: %d\n",cutPieces);
			cutPieces-=5;



		}else{

			//waitpid(p[3], &e,0); // Pai espera que o 4º filho termine
			int read2;
			read(m4a1[0],&read2,sizeof(read2));
			total_pieces-=100;
			stored_parts +=read2;
			printf("Stored Parts: %d\n",stored_parts);

		}
	}while(total_pieces>0);

	exit(0);

}


int main(){
	//pl2_ex1();
	//pl2_ex2a();
	//pl2_ex2b();
	//pl2_ex3();
	//pl2_ex4();
	//pl2_ex5();
	//pl2_ex6();
	//pl2_ex7();
	//pl2_ex8();

	//pl2_ex9();
	//pl2_ex10();

	//pl2_ex11();
	//pl2_ex12();
	pl2_ex13();


	return 0;
}
